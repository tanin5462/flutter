import 'package:flutter/material.dart';

import './product_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('EasyList'),
          ), //AppBar
          body: ProductManager()), //Scaffold
    ); //MaterialApp
  }
}
