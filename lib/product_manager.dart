import 'package:flutter/material.dart';
import './products.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;
  ProductManager(this.startingProduct);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [
    'Food Tester'
  ]; //เหมือนประกาศว่า product = [] List หมายถึง Array String คือ type ที่กำหนดว่า ใน List จะเป็น Type อะไร
  //การใส่ _ เหมือน scoep ให้ ตัวแปรนั้นๆสามารถใช้ได้ภายในไฟล์ ไฟล์เดียว
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(1.0),
          child: RaisedButton(
            onPressed: () {
              setState(() {
                //setState จะทำให้ flutter รับรู้ว่ามีการเปลี่ยนแปลง มันจะได้ทำการ build ให้ใหม่
                _products.add('Advanced Food Tester');
              });
            },
            child: Text('Add Product'),
          ),
        ),
        Products(_products)
      ],
    );
  }
}
